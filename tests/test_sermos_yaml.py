""" Tests confirming proper sermos.yaml formatting / parsing.
"""
from sermos.sermos_yaml import load_sermos_config


class TestSermosYaml:
    """ Test classes in utils/config_utils.py
    """
    def test_yaml_load(self):
        config = load_sermos_config('sermos_demo_client', 'sermos.yaml')
        assert config is not None