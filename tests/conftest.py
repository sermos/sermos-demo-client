""" Test setup
"""
from functools import wraps
import pytest
from mock import patch


def mock_auth(fn):
    """ Return function back unchanged.
    """
    @wraps(fn)
    def wrapped(*args, **kwargs):
        return fn(*args, **kwargs)

    return wrapped


@pytest.fixture(scope='session')
def app(request):
    """Session-wide test `Flask` application."""

    # Patch the require_apikey decorator so we don't actually
    # check api key when unit testing api routes.
    with patch('sermos.flask.decorators.require_accesskey', mock_auth):
        from sermos_demo_client.app import create_app
        flask_app = create_app()

        ctx = flask_app.app_context()
        ctx.push()

        def teardown():
            ctx.pop()

        request.addfinalizer(teardown)

        yield flask_app


@pytest.fixture
def client(app):
    """ Test flask client
    """
    return app.test_client()
