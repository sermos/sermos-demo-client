""" Test the Demo APIs
"""
import mock
from importlib import reload
from mock import patch
from functools import wraps


class TestDemoApiClass:
    def test_demo_api_class_post(self, app, client):
        """ Validate the demo api post endpoint works
        """
        with app.test_request_context():
            from sermos_demo_client.api.routes_api import DecoratedApiClass, \
                DecoratedFeatureRequestSchema, DecoratedFeatureResponseSchema
            my_post = {'compare_for_equality': True}

            # Assert schema validation
            validated = DecoratedFeatureRequestSchema().load(my_post)
            assert my_post == validated

            resp = client.post('api/v1/decorated/string-compare/foo/bar',
                               json=validated)  # Different strings

            validated_response = DecoratedFeatureResponseSchema().load(
                resp.json)
            assert resp.json == validated_response
            assert validated_response['comparison_result'] is False
            assert validated_response['compare_for_equality'] is True

            # Verify strings that are indeed the same
            resp = client.post('api/v1/decorated/string-compare/foo/foo',
                               json=validated)  # Same strings
            assert resp.json['comparison_result'] is True

            # Verify checking for inequality
            resp = client.post('api/v1/decorated/string-compare/foo/foo',
                               json={'compare_for_equality':
                                     False})  # Same strings
            assert resp.json['comparison_result'] is False
