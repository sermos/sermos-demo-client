""" Demo application using Sermos
"""
import re
import ast
from setuptools import setup, find_packages

_version_re = re.compile(r'__version__\s+=\s+(.*)')

with open('sermos_demo_client/__init__.py', 'rb') as f:
    __version__ = str(
        ast.literal_eval(
            _version_re.search(f.read().decode('utf-8')).group(1)))

setup(
    name='sermos-demo-client',
    version=__version__,
    description="An example Flask application using Sermos and Sermos Tools",
    long_description=open('README.md', 'r').read(),
    maintainer="Sermos, LLC",
    license="MIT",
    url="https://gitlab.com/sermos/sermos-demo-client",
    packages=find_packages(exclude=["tests"]),
    include_package_data=True,
    install_requires=[
        'flask-smorest==0.23.0',  # Bug in RhoWeb after this version of FlaskSmorest
        'sermos[flask,flask_auth,deploy,web,workers]>=0.84.0,<0.85.0',
        'sermos_tools[date_extractor,hasher,language_detector,text_extractor]>=0.2.0'
    ],
    extras_require={
        'build': ['twine', 'wheel'],
        'special_tool': ['cytoolz>=0.9,<1.0'],
        'dev': ['honcho'],
        'deploy': ['honcho', 'sermos[deploy]>=0.83.0'],
        'test': [
            'pytest-cov>=2.6.1,<3', 'tox>=3.14.1,<4', 'coverage>=4.5,<5',
            'mock>=2,<3'
        ],
        'test-ci': ['tox>=3.14.1,<4']
    },
    cmdclass={})
