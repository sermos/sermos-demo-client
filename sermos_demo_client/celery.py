""" Demo Application's Celery Configuration

Primary (recommended) run mode using Celery:

    celery -A sermos_demo_client.celery worker {args}

Optional (alternative) run mode as a module:

    python -m sermos_demo_client.celery worker {args}

"""
import sys
import logging
from sermos.celery import configure_celery, Celery

logger = logging.getLogger(__name__)
celery = configure_celery(Celery('sermos-celery-app'))

if __name__ == '__main__':
    """ Allow running Celery as module.
    """
    args = ['celery'] + sys.argv[1:]
    celery.start(argv=args)
