import os
import pickle
from typing import Any

from rho_ml import RhoModel, Version, store_rho_model, load_rho_model
from sermos_demo_client.utils.utils import get_correct_admin_api_endpoint

DEMO_MODEL_VERSION = os.environ.get('DEMO_MODEL_VERSION', '0.0.0')


class DemoModel(RhoModel):
    def __init__(self, some_hyper_param: Any, *args, **kwargs):
        # or use attrs to avoid __init__ boilerplate
        super(DemoModel, self).__init__(*args, **kwargs)
        self.some_hyper_param = some_hyper_param

    def validate_prediction_input(self, data: Any):
        if True:
            print("Data looks good!")

    def validate_prediction_output(self, data: Any):
        if True:
            print('Great prediction!')

    def predict_logic(self, prediction_data: Any, *args, **kwargs) -> Any:
        model_output = "Cool data!"
        print(model_output)
        return model_output

    def serialize(self) -> bytes:
        serialized_model = pickle.dumps(self, protocol=4)
        return serialized_model

    @classmethod
    def deserialize(cls, serialized: bytes):
        return pickle.loads(serialized)


def load_demo_model() -> DemoModel:
    """ Uses Sermos storage and deserialization to retrieve a RhoModel
        from cloud storage.

        Note: this also uses SERMOS_ACCESS_KEY from the environment.
    """
    model_name = 'DemoModel'
    version_pattern = "0.0.*"

    base_url = get_correct_admin_api_endpoint()
    get_model_endpoint = base_url + 'models/store-credentials'
    model = load_rho_model(model_name=model_name,
                           version_pattern=version_pattern,
                           get_model_endpoint=get_model_endpoint)
    return model


if __name__ == '__main__':
    fake_prediction_data = "Hello!"
    # create and store model:
    version = Version.from_string(DEMO_MODEL_VERSION)
    model_1 = DemoModel(some_hyper_param=42, version=version)
    model_1.predict(fake_prediction_data, run_validation=True)
    store_rho_model(model=model_1)
    print("Model saved!")

    # retrieve and deserialize the model
    model_2 = load_demo_model()
    model_2.predict(fake_prediction_data, run_validation=True)
