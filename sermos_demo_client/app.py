""" Demo Flask Applications to be deployed into Sermos.

    Sermos allows you to start one or more 'applications' in your `appConfig`.
    These are then managed for resources, scale, etc. based on your settings
    in addition to the ingress.

    appConfig:
      - name: Primary Web Server
        application: sermos_demo_client.app:create_app()
        ingress: /
      - name: Secondary API Server
        application: sermos_demo_client.app:create_app('api-public')
        ingress: /api/public

"""
import os
from flask import Flask
from sermos.flask import FlaskSermos, Api
from sermos.constants import DEFAULT_OPENAPI_CONFIG
from sermos_demo_client.api.routes_app import bp as bp_app
from sermos_demo_client.api.routes_api import bp as bp_api
from sermos_demo_client.api.routes_api_public import bp as bp_api_public
from sermos_demo_client.config import Config


def create_app(name: str = None):
    if name is None:
        name = __name__

    app = Flask(__name__)

    # Initialize your app with a configuration
    app.config.from_object(Config)

    # The Demo Client showcases several 'run modes.' In this case, the demo
    # application has a 'public api' that is intended to be exposed publicly
    # and not operate behind Rho Auth. It also has 'internal' API endpoints
    # and an 'internal' web application that does run behind Rho Auth and shares
    # the default swagger documentation.
    if name == 'api-public':
        # Initialize Sermos Flask Extension without Sermos API
        sermos = FlaskSermos()
        sermos.init_app(app)

        # Use Sermos provided default configuration options for Swagger UI
        for conf in DEFAULT_OPENAPI_CONFIG:
            app.config[conf[0]] = app.config.get(conf[0], conf[1])

        app.config['OPENAPI_URL_PREFIX'] = '/api/public'

        # Manually initialize Api
        api = Api(
            spec_kwargs={
                'title': "PUBLIC API",
                'version': "PUBLIC VERSION",
                'description': "PUBLIC DESCRIPTION",
                'tags': []
            })
        api.init_app(app)

        # Add relevant blueprints to API
        api.register_blueprint(bp_api_public)

    else:
        # Initialize Sermos Flask Extension using default settings that
        # include core Sermos API endpoints and Swagger UI at /api/v1/docs
        sermos = FlaskSermos()
        sermos.init_app(app, init_api=True)

        # Register API blueprints onto the core Sermos API
        app.extensions['sermos_core_api'].register_blueprint(bp_api)

        # Register application blueprints to application
        app.register_blueprint(bp_app)

    return app
