""" Demo only
"""
import logging
import time
from rho_ml import Version, store_rho_model
from sermos.utils.task_utils import TaskRunner, PipelineResult
from sermos_tools.catalog.language_detector import LanguageDetector
from sermos_tools.catalog.date_extractor import DateExtractor
from sermos_demo_client.models.demo_model import DemoModel, load_demo_model, \
    DEMO_MODEL_VERSION
from sermos_demo_client.utils.utils import get_correct_admin_api_endpoint

logger = logging.getLogger(__name__)


def demo_coordinator_task(event):
    """ Intended to be invoked on a schedule.
    """
    logger.info("RUNNING demo_coordinator_task ...")

    # Generate work for downstream task
    payload_per_worker = [{
        'task_payload':
        'My payload value number 1 - Jan 2, 2020'
    }, {
        'task_payload': 'My payload value number 2'
    }, {
        'task_payload': 'My payload value number 3'
    }, {
        'task_payload': 'My Nov, 2019 payload value number 4'
    }, {
        'task_payload': 'Mi numero de valor de carga 5'
    }, {
        'task_payload': 'My payload value number 6'
    }]

    # Issue the work in batches to take advantage of parallel execution
    TaskRunner.publish_work_in_batches(
        task_path='sermos_demo_client.workers.demo_worker.demo_worker_task',
        task_payload_list=payload_per_worker,
        queue='default-task-queue',
        grouping_key='custom_group',
        max_per_task=2)

    # Issue a single task. In this example, we're demonstrating publishing a
    # task in the same format `publish_work_in_batches` works (with a grouping
    # key that has a list of payloads). The task payload can be _anything_,
    # though. It's up to the designer to ensure receiving tasks know what to
    # expect.
    TaskRunner.publish_work(
        task_path='sermos_demo_client.workers.demo_worker.demo_worker_task',
        task_payload={
            'custom_group': [{
                'task_payload': 'Issued as a single task'
            }]
        },
        queue='default-task-queue')

    return True


def demo_worker_task(event):
    """ Intended to be invoked by a coordinator and/or as part of a pipeline.
    """
    logger.info("RUNNING demo_worker_task ...")

    # Able to handle tasks from batch with a known custom grouping key
    if 'custom_group' in event:
        for task in event['custom_group']:
            is_english = LanguageDetector(task['task_payload']).is_english()
            dates = DateExtractor(
                full_text=task['task_payload']).normalized_dates
            logger.info("Value '{}' is english: {}. Dates Found: {}".format(
                task['task_payload'], is_english, dates))

    time.sleep(2)
    return True


def demo_model_task(event):
    """ Contrived example of storing and loading a model in a task.

        NOTE: In practice, you would almost never store the model in a task,
        instead the model will have been built and stored elsewhere and a task
        like this will simply 'use' the model.
    """
    logger.info("RUNNING demo_model_task ...")

    fake_prediction_data = "Hello!"
    # Create and store model --> This will NOT be in the same task that runs
    # the prediction in most use cases.
    version = Version.from_string(DEMO_MODEL_VERSION)
    model_1 = DemoModel(some_hyper_param=42, version=version)
    base_url = get_correct_admin_api_endpoint()
    store_model_endpoint = base_url + 'models/store-credentials'
    store_rho_model(model=model_1, store_model_endpoint=store_model_endpoint)
    print("Model saved!")

    # Retrieve and deserialize the model
    model_2 = load_demo_model()
    output = model_2.predict(fake_prediction_data, run_validation=True)

    print("Model invocation successful: {}".format(output))

    try:
        pipeline_result = PipelineResult.from_event(event)
        pipeline_result.save(result={'model_output': output})
    except ValueError:
        logger.warning("Attempted to save a pipeline result when not in a "
                       "pipeline ...")

    time.sleep(2)
    return True
