""" Demo only
"""
import time
import logging
from sermos.utils.task_utils import PipelineRunWrapper, PipelineResult
logger = logging.getLogger(__name__)


def demo_pipeline_node_a(event):
    """ Invoked through the Sermos `invoke-pipeline` API endpoint.
    """
    logger.info("")
    logger.info("RUNNING demo_pipeline_node_a: {}".format(event))
    logger.info("")

    # Load the pipeline_run_wrapper from the event.
    pipeline_wrapper = PipelineRunWrapper.from_event(event)

    # Add some new data that is for use in a downstream node
    pipeline_wrapper.chain_payload['payload_for_node_b'] = {'source': 'Node A'}

    # Save current state of pipeline_run_wrapper with new chain payload
    pipeline_wrapper.save_to_cache()

    time.sleep(5)
    return True


def demo_pipeline_node_b(event):
    """ Can be invoked through API but, in this example, intended to be a node
        in a pipeline.
    """
    logger.info("")
    logger.info("RUNNING demo_pipeline_node_b: {}".format(event))
    logger.info("")

    # Load the pipeline_run_wrapper from the event.
    pipeline_wrapper = PipelineRunWrapper.from_event(event)

    # Show the new chain payload including `payload_for_node_b` for use here.
    logger.info("Chain payload: {}".format(pipeline_wrapper.chain_payload))

    time.sleep(5)
    return True


def demo_pipeline_node_c(event):
    """ Can be invoked through API but, in this example, intended to be a node
        in a pipeline.
    """
    logger.info("")
    logger.info("RUNNING demo_pipeline_node_c: {}".format(event))
    logger.info("")

    # Load the pipeline_run_wrapper from the event.
    pipeline_wrapper = PipelineRunWrapper.from_event(event)

    logger.info(f"Got the wrapper: {pipeline_wrapper}")
    pipeline_result = PipelineResult.from_event(event)
    pipeline_result.save(
        result={'custom_result': 'This is a custom result dictionary ...'})

    return True
