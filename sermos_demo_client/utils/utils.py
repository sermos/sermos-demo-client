""" Basic utils for demo purposes
"""
import os
from ..config import Config


def get_correct_admin_api_endpoint() -> str:
    """ Typical deployments should *always* use default endpoints.

        This demo runs against multiple environments so we specify the
        api endpoints directly.
    """
    return Config.SERMOS_BASE_URL + '/api/v1/'
