""" Flask Application Configuration
"""
import os
from sermos_demo_client import __version__


class Config:
    """ Basic configuration
    """

    SERMOS_CLIENT_VERSION = __version__
    RHOAUTH_ENABLED = bool(
        os.environ.get('RHOAUTH_ENABLED', 'true').lower() == 'true')
    SECRET_KEY = os.getenv('FLASK_SECRET_KEY', None)
    SERMOS_BASE_URL = os.getenv('SERMOS_BASE_URL', None)
