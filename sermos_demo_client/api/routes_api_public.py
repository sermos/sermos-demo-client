""" Demo application routes
"""
import logging
from flask.views import MethodView
from marshmallow import Schema, fields
from sermos.flask import Blueprint
from sermos.constants import API_DOC_RESPONSES, API_DOC_PARAMS,\
    API_PATH_V1
logger = logging.getLogger(__name__)

bp = Blueprint('public_api_routes', __name__, url_prefix='/api/public')


class PublicRouteSchema(Schema):
    message = fields.String(description='The response!',
                            example="Some Value!",
                            required=False)


@bp.route('/my-public-route')
class PublicRoute(MethodView):
    @bp.doc(parameters=[API_DOC_PARAMS['accesskey']],
            responses=API_DOC_RESPONSES,
            tags=['Public Routes'])
    @bp.response(PublicRouteSchema)
    def get(self):
        return {'message': 'Howdy!', 'unmarshalled': 'Excluded from response.'}
