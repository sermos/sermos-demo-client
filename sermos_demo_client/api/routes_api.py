""" Demo application routes
"""
import logging
from flask.views import MethodView
from sermos_tools.catalog.language_detector import LanguageDetector
from marshmallow import Schema, fields
from sermos.flask import Blueprint, abort
from sermos.flask.decorators import require_accesskey
from sermos.constants import API_DOC_RESPONSES, API_DOC_PARAMS,\
    API_PATH_V1

logger = logging.getLogger(__name__)
bp = Blueprint('api_routes', __name__, url_prefix=API_PATH_V1 + '/decorated')


class DecoratedFeatureRequestSchema(Schema):
    compare_for_equality = fields.Boolean(
        description='Check for equality (True) or inequality (False)',
        example=True,
        default=True,
        required=False)


class DecoratedFeatureResponseSchema(Schema):
    comparison_result = fields.Boolean(
        description='Whether provided strings satisfied the equality compare.',
        example=True,
        required=True)
    compare_for_equality = fields.Boolean(
        description='Whether strings were checked for for equality (True) or '
        'inequality (False)',
        example=True,
        required=True)
    is_english = fields.Bool(
        description='Whether strings were deemed to be English',
        example=True,
        required=False)


@bp.route('/string-compare/<string:string_1>/<string:string_2>')
class DecoratedApiClass(MethodView):
    """ Demo for how to define methods that can be used as Sermos API Endpoints
    """
    @bp.doc(parameters=[API_DOC_PARAMS['accesskey']],
            responses=API_DOC_RESPONSES,
            tags=['Decorated'])
    @bp.arguments(DecoratedFeatureRequestSchema)
    @bp.response(DecoratedFeatureResponseSchema)
    @require_accesskey
    def post(self, payload: dict, string_1: str, string_2: str):
        """ Basic `post` example with a payload and url parameters
        """
        compare_for_equality = payload.get('compare_for_equality', True)
        if compare_for_equality:
            match = string_1 == string_2
        else:
            match = string_1 != string_2
        return {
            'comparison_result': match,
            'compare_for_equality': compare_for_equality,
        }

    @bp.doc(parameters=[API_DOC_PARAMS['accesskey']],
            responses=API_DOC_RESPONSES,
            tags=['Decorated'])
    @bp.response(DecoratedFeatureResponseSchema)
    @require_accesskey
    def get(self, string_1: str, string_2: str):
        """ Basic `get` example.
        """
        # Demonstrate abort codes
        abort_codes = [400, 401, 403, 404, 500]
        for code in abort_codes:
            if string_1 == str(code) or string_2 == str(code):
                abort(code, "Custom Abort Message for a {}".format(code))

        match = string_1 == string_2

        is_english = LanguageDetector("{} {}".format(string_1,
                                                     string_2)).is_english()

        return {
            'comparison_result': match,
            'compare_for_equality': True,
            'is_english': is_english
        }
