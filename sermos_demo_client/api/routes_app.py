""" Demo application routes
"""
import logging
from flask import current_app, url_for
from sermos_tools.catalog.hasher import Hasher
from sermos.flask import Blueprint
from sermos.flask.decorators import require_login

logger = logging.getLogger(__name__)
bp = Blueprint('demo_routes', __name__)


@bp.route('/')
@require_login
def index():
    retval = "Index Route. See /app and /site-map for other routes ... "
    text_hash = Hasher().hash_string('Hi there, this is some text!')
    retval += f"Text Hash: {text_hash} "
    retval += "<br><hr><br><a href='/api/v1/docs'>Click Here for API</a>"
    return retval


@bp.route('/app')
@require_login
def application_1():
    return 'Another Route'


@bp.route("/site-map")
@require_login
def site_map():
    """ Demo route to show the 'site map' of this flask application.
    """
    def _has_no_empty_params(rule):
        """ Convenience method
        """
        defaults = rule.defaults if rule.defaults is not None else ()
        arguments = rule.arguments if rule.arguments is not None else ()
        return len(defaults) >= len(arguments)

    links = []
    for rule in current_app.url_map.iter_rules():
        # Filter out rules we can't navigate to in a browser
        # and rules that require parameters
        if "GET" in rule.methods and _has_no_empty_params(rule):
            url = url_for(rule.endpoint, **(rule.defaults or {}))
            links.append((url, rule.endpoint))

    # links is now a list of url, endpoint tuples
    retval = ""
    for link in links:
        retval += f"{link[0]}  ::  {link[1]}<br><br>"
    return retval
