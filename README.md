# Sermos Demo Client

[Sermos](https://gitlab.com/sermos/sermos) and
[Sermos Tools](https://gitlab.com/sermos/sermos-tools) provide a set of tools and a design roadmap
for developers to quickly and effectively integrate Data Science into real-world applications.

This repo is a demonstration of *one way* you can utilize these libraries. Additionally, this demo
shows off how to launch production-grade deployments in minutes [using Sermos Cloud](https://sermos.ai).
For the demonstration, we are showing how to build a Sermos application using our basic Flask extension.
This allows us to quickly build API endpoints, schedule tasks, and pipeline workers.

## 5-Minute-Or-Less Quickstart

In under 5 minutes, let's get a basic web application and set of API endpoints up and running.

1. If using [pyenv](https://github.com/pyenv/pyenv) (strongly recommended), install the version of
Python listed in `.python-version`
1. Create a new [Virtual Environment](https://docs.python.org/3/library/venv.html)
1. Install the application locally: `pip install -e .[special_tool,dev,test]`
1. Follow brief instructions inside of: `dev/example.env`
1. `cd dev`
1. Run your Flask App (includes the API and basic web app): `honcho start -f bin/procfiles/Procfile.web`
1. Navigate to `localhost:5000` in your web browser
1. Virtual high-five!

## Tasks and Pipelines Quickstart

Now that you have a functional local development environment, let's demonstrate some scheduled tasks
and pipelines that you might use in a real production workload.

1. `cd dev`
1. Ensure your web application and APIs are running: `honcho start -f bin/procfiles/Procfile.web`
1. Start the two required services for a standard deployment: `Redis` and `RabbitMQ`
  * You can start these however you would like
  * We've provided a `docker-compose` file for convenience in the `/dev` directory
  * To use: `docker-compose -f sandbox-dbs.yaml up`
1. Start your local configuration server: `sermos local-config-api`
  * This mocks some services of [Sermos Cloud](https://sermos.ai) for schedules and pipelines
1. Start your Workers: `honcho start -f bin/procfiles/Procfile.worker`
1. Start your 'beat': `honcho start -f bin/procfiles/Procfile.beat`
  * This is your task scheduler if following a standard system design

## Dockerized Tasks and Pipelines Quickstart

We strongly recommend containerizing your applications whether or not you use
[Sermos Cloud](https://sermos.ai). When deploying using Docker, we recommend that you test your
containerized application locally before deploying as it will save you a *lot* of debugging
headache. Trust us.

This demo repo provides examples on how you can build your containerized application consistently
and it also has a Gitlab CI configuration that demonstrates how you can utilize CI/CD tools
for consistent builds and deploys. To build and use locally:

1. Review the files inside `/docker` and update if necessary (they should work out of the box, though)
1. Build a local egg locally for this application: `python setup.py sdist`
1. Build a local image: `./docker/build-image.sh -d -t my-great-tag`

## Repo Contents

Below is an overview of some repository contents of note.

```
sermos-demo-client
|   .sops.yaml               --> Keep your secrets secret! We like SOPS.
|   setup.py                 --> See the basic dependencies.
|
`---bin
|   `---procfiles            --> honcho start -f bin/procfiles/Procfile.*
|      |   Procfile.beat
|      |   Procfile.web
|      |   Procfile.worker
`---dev                      --> Example files used for *local development*
│   │   example.env          --> Rename this to .env for keeping local dev secrets
│   │   pipelines.yaml       --> Pipelines configuration file
|   |   sandbox-app.yaml     --> API and config server using docker-compose
|   |   sandbox-dbs.yaml     --> Databases using docker-compose
|   |   sandbox-workers.yaml --> Workers using docker-compose
|   |   schedules.json       --> Scheduled tasks configuration file
`---docker                   --> Example of how you can build your own app using Docker
|   |   build-image.sh       --> Build dev and prod Docker images from templates
|   |   Dockerfile-dev.tmpl  --> Development template
|   |   Dockerfile.tmpl      --> Production template
`---secrets
|   |   *.env                --> Encrypted secrets using SOPS, contain deployment secrets
`---sermos_demo_client       --> Soon to be YOUR Sermos application!
|   `---api
|      |   *.py              --> Examples for building Sermos-powered API endpoints
|   `---models
|      |   *.py              --> Example "data science model" using RhoML
|   `---utils
|      |   *.py              --> Extremely basic utility methods
|   `---workers
|      |   *.py              --> Example scheduled task and pipeline worker methods
|   |   app.py               --> Basic Flask application entrypoint
|   |   celery.py            --> Basic Celery application configured using Sermos
|   |   config.py            --> Basic application configuration
|   |   sermos.yaml          --> Simple config file used when deploying to Sermos Cloud
```

## Testing

To run the tests, ensure you've installed the `test` extras
(`pip install -e .[test]`). Sermos currently supports Python 3.7+.

Run tests for all tools:

    $ tox
