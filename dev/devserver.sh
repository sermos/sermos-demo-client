#!/bin/bash
BASE_DIR=`dirname $0`
WORKING_DIR=`pwd`
echo ""
echo "Development script to start a customer Sermos app locally."
echo "to be installed locally in this virtual environment."
echo "--------------------------------------------------------------------"
# Default parameters
#
HOST="127.0.0.1"
PORT="5000"
PROCFILE_PATH="/bin/procfiles/Procfile.web"
PROCFILE=$WORKING_DIR$PROCFILE_PATH
ENVFILE="secrets/dev.env"
# Parse the command line flags.
#
while getopts ":np:e:f:" opt; do
  case $opt in
    n)
      # Get the IP address on a mac.  Only works on a mac.
      #
      HOST=`ifconfig | grep -E 'inet.[0-9]' | grep -v '127.0.0.1' | awk '{ print $2}'|head -n1`
      ;;
    p)
      # Set the port
      #
      PORT=${OPTARG}
      ;;
    e)
      # Set the environment file
      ENVFILE=${OPTARG}
      if [[ ! -e "$ENVFILE" ]]; then
        die "... your specified $ENVFILE does not exist"
      fi
      ;;
    f)
      # Set the Procfile
      #
      PROCFILE=${OPTARG}
      if [[ ! -e "$PROCFILE" ]]; then
        die "...your specified $PROCFILE does not exist"
      fi
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
  esac
done
# Custom die function.
#
die() { echo >&2 -e "\nRUN ERROR: $@\n"; exit 1; }
# Check for required programs: coffee, redis-server, and Postgres.app
#
HONCHO=$(command -v honcho || command -v foreman || die "...Error: honcho/foreman is not in your path!  Are you in the right virtualenv?")
# Print config
#
echo ""
echo "Configuration:"
echo -e "\tPROCFILE: $PROCFILE"
echo -e "\tENVFILE: $ENVFILE"
echo -e "\tHONCHO: $HONCHO"
echo -e "\tHOST: $HOST"
echo -e "\tPORT: $PORT"
echo -e "\tPATH: $PATH"
echo ""
echo "-------------------------------------------------------------------"
# Start the other processes.  See bin/Procfile.dev
#
sops -d $ENVFILE > .env.sops
HOST=$HOST PORT=$PORT $HONCHO start -e .env.sops -f $PROCFILE
rm .env.sops
