# Sermos Demo Client Release Notes

## v0.17.1 (2021-06-04)
* Update API service id.

## v0.17.0 (2021-06-03)
* Update build pipeline
* Bump to latest Sermos with new deploy CLI

## v0.16.1 (2021-03-03)
* Bump min sermos version

## v0.16.0 (2021-03-03)
* Updated test running
* Migration to latest Sermos
* Updated deployment secrets
* Updated development scripts

## v0.15.0 (2020-11-19)
* Initial builds in Gitlab CI
* New docker build process for building both prod and staging images
* Non-functional 'deploy' steps in CI atm
* Happy birthday Tyman!

## v0.14.0 (2020-07-16)
* Bump to latest sermos utils and specify API replica count/cpu/mem requests.

## v0.13.0 (2020-05-20)
* Bump to latest sermos-utils and rename SERMOS_DEPLOY_KEY to SERMOS_ACCESS_KEY

## v0.12.0 (2020-04-15)
* Update The sermos-demo-staging.env file to use a deploy key specifically on
staging and to specify the DEMO_ENVIRONMENT.
* Update the bitbucket pipeline to use the new -staging.env file for
release deployments that contact the admin-staging server
* Inject DEMO_ENVIRNMENT, DEMO_MODEL_VERSION into sandbox workers
* Inject DEMO_ENVIRONMENT into global environment in sermos.yaml, default to
staging, overloaded by respective .env files
* New utility for using the DEMO_ENVIRONMENT to properly switch between admin
api endpoints
* Update demo_model.py and demo_worker.py to use new
`get_correct_admin_api_endpoint()`

## v0.11.2 (2020-03-20)
* Bugfixes for model worker

## v0.11.1 (2020-03-20)
* Bugfixes for model storage example

## v0.11.0 (2020-03-20)
* Bump sermos-utils and rho-ml
* Updates to RhoModel creation and versioning

## v0.10.3 (2020-02-22)
* Split requirements files
* Local dev initial setup

## v0.10.2 (2020-02-18)
* Remove a verbose log and add a try/except so we can use the same method
in a pipeline and a worker for demo
* Rename a schema to avoid warnings about namespace conflict

## v0.10.1 (2020-02-01)
* Dev version bump

## v0.10.0 (2020-01-31)
* Bump Sermos Utils to use new imageConfig functionality.
* Update sermos.yaml to show off multi-image.

## v0.9.5 (2020-01-21)
* Bump to latest sermos-utils

## v0.9.4 (2020-01-21)
* Add new release branch deployment to staging + new demo-staging secrets.

## v0.9.3 (2020-01-21)
* Add sermos utils to deploy extra

## v0.9.2 (2020-01-21)
* Bugfix to tox config

## v0.9.1 (2020-01-21)
* Bump sermos-utils requirement

## v0.9.0 (2020-01-21)
* Move requirements into a `core` extra to demonstrate new pipInstallCommand
option in sermos.yaml

## v0.8.2 (2020-01-14)
* Bump version of Rho ML and remove accidental commit hardcoding local base path

## v0.8.1 (2020-01-14)
* Added a new "model task" for demoing in scheduled tasks and pipelines.

## v0.8.0 (2020-01-14)
* Bump Rho ML and Sermos Utils versions
* Use default endpoints for admin api
* Version pattern re-added with api search implemented

## v0.7.2 (2020-01-13)
* Bump sermos utils for Deploy url bugfix

## v0.7.1 (2020-01-13)
* Test bump for build pipeline.

## v0.7.0 (2020-01-13)
* Workaround for missing api pattern matching
* Added demo model creation, saving, and loading
* Bump sermos utils bugfix version.

## v0.6.0 (2020-01-08)
* Attempt for issuing a sermos deploy on a version build.

## v0.5.0 (2020-01-08)
* Bump version of sermos-utils
* Add examples of using environment variables across
global/api/workers in sermos.yaml

## v0.4.0 (2019-12-11)
* Use Sermos Utils for deployment, replacing bin/deploy.py script.

## v0.3.0 (2019-12-05)
* Update sermos.yaml with updated format in Sermos 0.60.0
* Small mods to pipeline demo with one additional node on a special queue.

## v0.2.2 (2019-12-03)
* Bug fix to deploy script for pipeline status

## v0.2.1 (2019-12-03)
* Move node_b to special-queue

## v0.2.0 (2019-12-01)
* Dev version bump

## v0.1.0 (2019-11-07)
* Initial functional demo client.
