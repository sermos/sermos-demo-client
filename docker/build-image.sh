#!/bin/bash

# This is an example "build script" to generate a local development image
# combining the latest Sermos build with your code. This is for demonstration
# purposes only.

echo ""
echo "Building Docker image"
echo "----------------------"

# Default parameters
#
DOCKER_FILE='./docker/Dockerfile.tmpl'
SED=$(command -v gsed || command -v sed)

# Custom die function.
#
die() { echo >&2 -e "\nRUN ERROR: $@\n"; usage; exit 1; }

usage()
{
cat << EOF
usage: ./build.sh [-h] [-d] [-n] [-t] [-v] [-p] [-i]

  OPTIONS:
   -h      Show this message.
   -d      Development mode.
   -n      The image name. Can exclude for dev mode.
   -t      The image tag. Required for dev mode.
   -v      Application version.
   -p      The Gitlab CI Token (password). Required for prod mode.
   -i      The Gitlab Project ID. Required for prod mode.
EOF
}

# Parse the command line flags.
#
while getopts "hdn:t:v:p:i:" opt; do
  case $opt in
    h)
      usage
      exit 1
      ;;
    d)
      DEV_MODE=true
      ;;
    n)
      IMAGE_NAME=${OPTARG}
      ;;
    t)
      IMAGE_TAG=${OPTARG}
      ;;
    v)
      VERSION=${OPTARG}
      ;;
    p)
      CI_JOB_TOKEN=${OPTARG}
      ;;
    i)
      CI_PROJECT_ID=${OPTARG}
      ;;
    \?)
      die "Invalid option: -$OPTARG"
      ;;
  esac
done

# Recreate the build directory
rm -r ./docker/build
mkdir ./docker/build

if [ "$DEV_MODE" = true ]; then
  echo "BUILDING IN DEV MODE ..."
  DOCKER_FILE="./docker/Dockerfile-dev.tmpl"
  
  # Copy docker file ".tmpl" file to the build context as build/Dockerfile
  cp $DOCKER_FILE ./docker/build/Dockerfile

  if [ -z ${IMAGE_NAME} ]; then
    IMAGE_NAME='sermos-local'
  fi

  if [ -z ${IMAGE_TAG} ]; then
    die "Must provide image tag in dev mode (e.g., -t COMMIT_HASH) ...";
  fi

  # NOTE: For development mode, ensure you've built the package locally `make build`
  cd ./dist  # Go into dist
  PKG_FILENAME=$(ls -t sermos-demo-client-*.tar.gz | head -1)  # Get latest
  if [ -z ${PKG_FILENAME} ]; then
      die "Could not find anything to build ...";
  fi
  echo "Retrieved egg filename: ${PKG_FILENAME}"
  cd ..  # Go back to project root directory and copy into build context
  cp ./dist/$PKG_FILENAME ./docker/build/$PKG_FILENAME

  echo "Building with tag :${IMAGE_TAG}..."
  cd ./docker
  docker build\
    -t="${IMAGE_NAME}:${IMAGE_TAG}"\
    --build-arg PKG_FILENAME=$PKG_FILENAME\
    build/
else
  echo "BUILDING IN PRODUCTION MODE ..."

  # Copy docker file ".tmpl" file to the build context as build/Dockerfile
  cp $DOCKER_FILE ./docker/build/Dockerfile

  if [ -z ${VERSION} ]; then
    die "Must provide application version in production mode (e.g., -v 0.1.0) ...";
  fi
  IMAGE_TAG="${VERSION}"

  if [ -z ${IMAGE_NAME} ]; then
    die "Must provide image name for production builds (e.g. sermos/sermos-demo-client) ...";
  fi

  if [ -z ${CI_JOB_TOKEN} ]; then
    die "Must provide job token for production builds ...";
  fi

  if [ -z ${CI_PROJECT_ID} ]; then
    die "Must provide Gitlab project id for production builds ...";
  fi

  echo "Building with tags :latest and :${IMAGE_TAG}..."
  cd ./docker
  docker build\
    -t="${IMAGE_NAME}:latest"\
    -t="${IMAGE_NAME}:${IMAGE_TAG}"\
    --build-arg VERSION=$VERSION\
    --build-arg CI_JOB_USER='gitlab-ci-token'\
    --build-arg CI_JOB_TOKEN=$CI_JOB_TOKEN\
    --build-arg CI_PROJECT_ID=$CI_PROJECT_ID\
    build/
fi

# Clean up the build directory
rm -rf ./build
cd ..
echo "Fin."
